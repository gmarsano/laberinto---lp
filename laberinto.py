# coding=utf8
from random import randint

class Tablero:
  __MAX_ROW = 5
  __MAX_COL = 6
  __WALL_V = { # Total verticales =  MAX_COL+1 partiendo de 0
    0: [1, 1, 1, 1, 1], # primera vertical x MAX_ROW filas; indice row parte en 0, no en 1!
    1: [0, 0, 0, 0, 0],
    2: [0, 0, 1, 1, 0],
    3: [0, 1, 0, 0, 0],
    4: [0, 1, 1, 1, 0],
    5: [0, 1, 1, 1, 0],
    6: [1, 1, 1, 1, 1]
  }
  __WALL_H = { # Total horizontales =  MAX_ROW+1 partiendo de 0
    0: [1, 1, 1, 1, 1, 1], # primera horizontal x MAX_COL columnas; indice col parte en 0, no en 1!
    1: [0, 1, 1, 0, 0, 0],
    2: [0, 1, 0, 0, 0, 0],
    3: [0, 0, 1, 1, 0, 0],
    4: [0, 1, 0, 1, 0, 0],
    5: [1, 1, 1, 1, 1, 1]
  }
  __COL_NAME = {
    1: "A", 2: "B", 3: "C", 4: "D",
    5: "E", 6: "F"
  }
  __ORIENT = {
    1: "^", 2: ">", 3: "v", 4: "<"
  }
  __row = 1
  __col = 1
  __orient = 2
  __goal = "D4"
  moves = ["A1>"]
  actions = 0

  def __set_row (self, val):
    if 0 < val < self.getMaxRow(): self.__row = val
    else: print "Error set_row"
  def __get_row (self):
    return self.__row
  row = property(__get_row, __set_row)

  def __set_col (self, val):
    if 0 < val < self.getMaxCol(): self.__col = val
    else: print "Error set_col"
  def __get_col (self):
    return self.__col
  col = property(__get_col, __set_col)

  def __set_orient (self, val):
    if val == 1:
      self.__orient += 1
      if self.__orient > 4: self.__orient = 1
    elif val == -1:
      self.__orient -= 1
      if self.__orient < 1: self.__orient = 4
    else: print "Error set_orient"
  def __get_orient (self):
    return self.__orient
  orient = property(__get_orient, __set_orient)
  
  def getColName (self):
    return self.__COL_NAME[self.__col]
  
  def drawOrient (self):
    return self.__ORIENT[self.__orient]

  def getMaxRow (self):
    return self.__MAX_ROW

  def getMaxCol (self):
    return self.__MAX_COL
  
  def getPos (self):
    return self.getColName()+str(self.row)

  def avanza (self):
    '''
    Avanza al robot una casilla
    '''
    if self.orient == 1:
      self.__row -= 1
    elif self.orient == 2:
      self.__col += 1
    elif self.orient == 3:
      self.__row += 1
    else:
      self.__col -=1
    self.actions += 1
    self.moves.append(self.getPos()+self.drawOrient())
  
  def giro (self, der = True):
    '''
    Gira a la derecha por defecto.
    Gira a la izq con argumento False
    '''
    if der: self.__set_orient(1)
    else: self.__set_orient(-1)
    self.actions += 1
    self.moves.append(self.drawOrient())

  def choque(self):
    '''
    Verifica si el robot chocará si avanza, según su
    orientación
    '''
    if self.orient == 1:
      return bool( self.__WALL_H[self.row - 1][self.col - 1] )
    elif self.orient == 2:
      return bool( self.__WALL_V[self.col][self.row - 1] )
    elif self.orient == 3:
      return bool( self.__WALL_H[self.row][self.col - 1] )
    else:
      return bool( self.__WALL_V[self.col - 1][self.row - 1] )
  
  def showMoves (self):
    i = 0
    for m in self.moves:
      i +=1
      if i % 10 == 0 or i == len(self.moves):
        print m
      else:
        print m,

  def isGoal (self):
    if self.getPos() == self.__goal: return True
    return False

##
 # MAIN
 ##
t = Tablero()
while not t.isGoal():
  if t.choque():
    t.giro(False)
  else:
    t.avanza()
    t.giro()
  
  if randint(1, 6) + randint(1, 6) == 12:
    t.giro(False)
    if not t.choque(): t.avanza()

t.showMoves()
print "Total de acciones: ",t.actions
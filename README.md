# Laberinto 2

![0001](./0001.jpg)

## Algoritmo

```mermaid
  graph TD
  n1(("Start"))
  n2{"¿Llego a la meta?"}
  n3{"choque ?"}
  n4["giro(-90°)"]
  n5["avanza(paso)"]
  n6["giro(90°)"]
  n7(("End"))
  n8{"dados == 12 ?"}
  n9["giro(-90°)"]
  n10{"choque ?"}
  n11["avanza(paso)"]
  n1 --> n2
  n2 -->|NO| n3
  n2 -->|SI| n7
  n3 -->|SI| n4
  n4 --> n8
  n3 -->|NO| n5
  n5 --> n6
  n6 --> n8
  n8 -->|SI| n9
  n8 -->|NO| n2
  n9 --> n10
  n10 -->|SI| n2
  n10 -->|NO| n11
  n11 --> n2
```

## Código

```python
while not isGoal():
  if choque:
    giro(-90)
  else:
    avanza()
    giro(90)
  
  if randint(1, 6) + randint(1, 6) == 12:
    giro(-90)
    if not choque(): avanza()
```